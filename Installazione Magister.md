
<img src="logomagis.svg" alt="drawing" width="100"/>

# Schema installazione Server Magister

## Table of Contents

1. [Server Applicazione Magister](#server-applicazione-magister)
2. [Server Api REST](#server-api-rest)
3. [Server Report](#server-report)
4. [Server Applicazione Magister Console](#server-applicazione-magister-console)

## Server Applicazione Magister

Contiene l'applicazione Front- End di Magister ed è installato come sottosito di default web site e risponde all'indirizzo http://serverlenovo/magister

### Fasi preliminari all'utilizzo dell'applicazione di front- end
Magister è una single page application realizzata in javascript e viene eseguita lato client. Per funzionare bisogna impostare nel local storage del browser (consigliato chrome) alcuni parametri:

Dopo essersi connessi dal browser alla pagina http://serverlenovo/magister bisogna , facendo click destro su un punto qualsiasi della pagina di login, selezionare la voce ispeziona.

Una volta aperto il developer tools di chrome selezionare la  voce "Application" dal menu principale e impostare , su storage -> localstorage -> http://serverlenovo le seguenti chiavi :

|Key|Value|
|---|---|
|apiurl|  http://serverlenovo:8081/api/|
|reportServerApiUrl|http://serverlenovo:8082/api/ReportApi|

## Server Api REST

Contiene la web api di back-end di Magister che provvede alla realizzazione di tutte le funzioni di accesso al database e allo storage dei dati.

**Configurazione:**

I dati di configurazione sono contenuti nel file "appsettings.json" contenuto nella directory virtuale che contiene le dll della web api di back-end (nel caso specifico sul serverlenovo nel percorso: "C:\inetpub\magisbackend").

```json
{
  "SecurityKey": "dd%88*377f6d&f£$$£$FdddFF33fssDG^!3",
  "Logging": {
    "IncludeScopes": false,
    "Debug": {
      "LogLevel": {
        "Default": "Warning"
      }
    },
    "Console": {
      "LogLevel": {
        "Default": "Warning"
      }
    }
  },
  "ConnectionStrings": {
    "ErpDbConnection": "Server=localhost\\SQLEXPRESS2017;database=magis;  MultipleActiveResultSets=True; Integrated Security=True;",
    "SystemConnection": "Server=localhost\\SQLEXPRESS2017;database=magistersystem;  MultipleActiveResultSets=True; Integrated Security=True;"
  },
  "MagoNetSettings": {
    "loginCompany": "nts",
    "loginInstallationName": "magonet",
    "loginPassword": "Gieffex3035",
    "loginServerMago": "localhost",
    "loginUsername": "sa",
    "percorsoAllegati": "e:\\allegatints\\",
    "reportServerApiUrl": "http://192.168.2.216:8089/api/ReportApi",
    "schemaXsdBolleName": "\"http://www.microarea.it/Schema/2004/Smart/ERP/Sales/DeliveryNotes/Standard/ddt.xsd\""
  }
}
```

La sezione che ci interessa è la **ConnectionStrings** in cui sono da impostare i valori dei database **ErpDbConnection** che è il database contenente i dati gestionali e **SystemConnection** che è il database che contiene i dati di sistema (utenti, istituti e utenti per istituto)

## Server Report

Contiene il servizio di gestione dei report che viene ospitato si iis nel sito "MagisReportApi" (C:\inetpub\wwwroot\MagisReportApi).

Questa Web Api verrè interrogata dal front-end e si occuperà di acquisire i parametri del report, eseguirlo e renderlo visualizzabile nel front-end.

**Configurazione**

I dati di configurazione sono contenuti nel file xml web.config contenuto nella cartelle della web api (C:\inetpub\wwwroot\MagisReportApi).

```xml
<?xml version="1.0" encoding="utf-8"?>
<!--
  For more information on how to configure your ASP.NET application, please visit
  https://go.microsoft.com/fwlink/?LinkId=301879
  -->
<configuration>
  <appSettings>
    <add key="dbuser" value="sa" />
    <add key="dbpassword" value="Gieffex3035" />
    <add key="dbconnectionstring" value="Data Source=localhost\sqlexpress2017;Initial Catalog=Magis;" />
  </appSettings>
  <system.web>
    <compilation targetFramework="4.5.2" />
    <httpRuntime targetFramework="4.5.2" />
  </system.web>
  <system.webServer>
    <handlers>
      <remove name="ExtensionlessUrlHandler-Integrated-4.0" />
      <remove name="OPTIONSVerbHandler" />
      <remove name="TRACEVerbHandler" />
      <add name="ExtensionlessUrlHandler-Integrated-4.0" path="*." verb="*" type="System.Web.Handlers.TransferRequestHandler" preCondition="integratedMode,runtimeVersionv4.0" />
    </handlers>
        <directoryBrowse enabled="true" />
  </system.webServer>
  <runtime>
    <assemblyBinding xmlns="urn:schemas-microsoft-com:asm.v1">
      <dependentAssembly>
        <assemblyIdentity name="System.Web.Helpers" publicKeyToken="31bf3856ad364e35" />
        <bindingRedirect oldVersion="1.0.0.0-3.0.0.0" newVersion="3.0.0.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="System.Web.Mvc" publicKeyToken="31bf3856ad364e35" />
        <bindingRedirect oldVersion="1.0.0.0-5.2.3.0" newVersion="5.2.3.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="System.Web.WebPages" publicKeyToken="31bf3856ad364e35" />
        <bindingRedirect oldVersion="1.0.0.0-3.0.0.0" newVersion="3.0.0.0" />
      </dependentAssembly>
    </assemblyBinding>
  </runtime>
  <system.codedom>
    <compilers>
      <compiler language="c#;cs;csharp" extension=".cs" type="Microsoft.CodeDom.Providers.DotNetCompilerPlatform.CSharpCodeProvider, Microsoft.CodeDom.Providers.DotNetCompilerPlatform, Version=1.0.3.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" warningLevel="4" compilerOptions="/langversion:6 /nowarn:1659;1699;1701" />
      <compiler language="vb;vbs;visualbasic;vbscript" extension=".vb" type="Microsoft.CodeDom.Providers.DotNetCompilerPlatform.VBCodeProvider, Microsoft.CodeDom.Providers.DotNetCompilerPlatform, Version=1.0.3.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" warningLevel="4" compilerOptions="/langversion:14 /nowarn:41008 /define:_MYTYPE=\&quot;Web\&quot; /optionInfer+" />
    </compilers>
  </system.codedom>
</configuration>
<!--ProjectGuid: 64DB6043-DCA3-4F2A-8F32-AC704E146BAA-->
```

La sezione da impostare è quella in cui sono contenute le chiavi e i valori necessari alla connessione usata dai report per leggere i dati. 

```xml
  <appSettings>
    <add key="dbuser" value="sa" />
    <add key="dbpassword" value="Gieffex3035" />
    <add key="dbconnectionstring" value="Data Source=localhost\sqlexpress2017;Initial Catalog=Magis;" />
  </appSettings>
```

## Server Applicazione Magister Console

Applicazione web contenente le funzionalità di gestione Utenti, istituti ed utenti per istituto. Risiede nella cartella "c:\inetpub\wwwroot\MagisterConsole" ed è accessibile solo al gruppo administrators del server. Il link è : http://serverlenovo/magisterconsole 

**Configurazione**

I dati di configurazione vengono letti dal file appsettings.json 

```json
{
  "Logging": {
    "LogLevel": {
      "Default": "Information",
      "Microsoft": "Warning",
      "Microsoft.Hosting.Lifetime": "Information"
    }
  },
  "BackEndSettings": {
    "Url": "http://localhost:8081"
  },
  "AllowedHosts": "*"
}
```

La chiave da impostare è "BackEndSettings" e contiene l'url dell web api di back-end
