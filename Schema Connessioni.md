# Schema connessioni Magister


## Table of Contents

1. [Connessioni VPN](#connessioni-vpn)
2. [Utenti Magister](#utenti-magister)
2. [Alawin](#alawin)

## Connessioni VPN

### Password amministratore firewall Pfsense

**User:** admin
**Password:** FmaGieffex3035

### Passwords Utenti OPNVPN 

|Utente|Password VPN|
|---|---|
|umberto.catania|Fma3035|
|laura.loreta|Fma3035|
|Maria.Margarone|Fma3035|
|Roberto.Chiaramonte|Fma3035|

## Utenti Magister

Credenziali di accesso alla procedura MAGISTER

|Utente|Password|Ist. codice|Ist. Descrizione|
|---|---|---|---|
|Umberto.Catania|fmauc001|001|ENTE GIURIDICO MADRE MAZZARELLO PALERMO|
|Laura.Loteta|fmall002|002|ENTE GIURIDICO ISTITUTO FEMM. S.G. BOSCO|
|Maria.Margarone|fmamm003|003|ENTE GIURIDICO ISTITUTO MARIA MAZZARELLO CATANIA|
|Roberto.Chiaramonte|fmarc004|004|ENTE GIURIDICO SAN GIOVANNI BOSCO |
|Antonio.Scandurra|Salvo2006|TEST|ENTE GIURIDICO TEST |

## Alawin

Credenziali di accesso alla procedura ALAWIN

|Utente|Password|
|---|---|
|dba|ala|
|fmamessina|fmamessina|
|fmapalermo|fmapalermo|

---